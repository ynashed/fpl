/*
 * Reconstruction.h
 *
 *  Created on: Sep 10, 2015
 *      Author: ynashed
 */

#ifndef RECONSTRUCTION_H_
#define RECONSTRUCTION_H_

#include "IConstraint.h"
#include "DPStack.h"
#include <functional>
#include <vector>

namespace fpl
{

struct RecConstraint
{
	IConstraint* constraint;
	int startIteration;
	int endIteration;
	int frequency;
	RecConstraint(IConstraint* c=0, int s=0, int e=0, int f=0) :
					constraint(c), startIteration(s), endIteration(e), frequency(f)
	{}
};

class Reconstruction
{
protected:
	af::array m_object;
	af::array m_previousObject;
	af::array m_support;
	af::array m_cachedDiffs;

	DPStack m_diffData;
	std::vector<RecConstraint> m_phasingConstraints;

	int m_iterationsNum;
	bool m_useCachedDiffs;

	void initObject(const af::array* guess=0);
	void initSupport(const af::array* guess=0);
	void clearConstraints();
	void reloadConstraints();

public:
	Reconstruction();
	virtual ~Reconstruction()					{clearConstraints();}

	af::dim4 shape() const 						{return m_object.dims();}

	void addConstraint(const char* p, int start, int end=-1, int freq=1)
	{
		if(freq<=0)
			return;
		if(end<start)
			end=start; //apply once
		int N = ((end-start)+1)/freq;
		m_phasingConstraints.push_back(	RecConstraint(IConstraint::createInstance(p,N),
										start,end,freq));
		if(end>m_iterationsNum)
			m_iterationsNum = end;
	}

	void reset(const af::array* guessO=0, const af::array* guessS=0)
	{
		initSupport(guessS);
		initObject(guessO);
		reloadConstraints();
	}
	void reloadData();
	void reconstruct();
	void save();
	void useCachedDiffs(bool flag)				  {m_useCachedDiffs=flag;}
	void updateGuess(const af::array& O);
	void updateSupport(const af::array& S)		  {m_support = S;}
	void updateCache(const af::array& I)		  {m_cachedDiffs = I;}
	const af::array& getGuess() 			const {return m_object;}
	const af::array& getPreviousGuess() 	const {return m_previousObject;}
	const af::array& getSupport() 			const {return m_support;}
	const af::array& getNonCachedDiffs() 	const {return m_diffData.getArray();}
	const af::array& getDiffractionData() 	const {return m_useCachedDiffs?m_cachedDiffs:m_diffData.getArray();}
	double getDiffractionPowerSpectrum()	const {return m_diffData.powerSpectrum();}
	double getModulusError()				const;
	double getSharpness() 					const;
	unsigned int getVolume(float t=0.2)		const;
};

} /* namespace fpl */

#endif /* RECONSTRUCTION_H_ */
