/*
 * IConstraint.cpp
 *
 *  Created on: Sep 29, 2015
 *      Author: ynashed
 */

#include "IConstraint.h"
#include "Reconstruction.h"
#include <stdio.h>
#include <fstream>
using namespace af;

namespace fpl
{

IConstraint* IConstraint::createInstance(const char* p, int N)
{
	std::stringstream ss(p);
	std::string methodName, params;
	getline(ss, methodName, ':');
	getline(ss, params);
	methodName = toLower(methodName);
	if(methodName == "modulus")
		return new ModulusConstraint(methodName,params,N);
	else if(methodName == "hio")
		return new HIO_Constraint(methodName,params,N);
	else if(methodName == "er")
		return new SupportConstraint(methodName,params,N);
	else if(methodName == "shrinkwrap")
		return new Shrinkwrap(methodName,params,N);
	else if(methodName == "lres")
		return new LRes(methodName,params,N);
	else if(methodName == "twindel")
		return new TwinDeletion(methodName,params,N);
	else
		return new ModulusConstraint(methodName,params,N); //ModulusConstraint is the default for non-matching method names
}

void ReciprocalspaceConstraint::apply(Reconstruction* R)
{
	array O = R->getGuess();
	const array& I = R->getDiffractionData();

	transform(O);
	float cerr = sum<float>( pow( (abs(O)-abs(I)), 2 ));
	update(O,I);
	inverse(O);

	R->updateGuess(O);
	m_error = cerr/R->getDiffractionPowerSpectrum();
#ifdef DEBUG_MSG
	printf("%e\n", m_error);
#endif
}

void SupportConstraint::apply(Reconstruction* R)
{
	array O = R->getGuess();
	const array& S = R->getSupport();

	float ps1 = sum<float>(pow(abs(O), 2), 0);
	O*=S; //Update real space object
	float ps2 = sum<float>(pow(abs(O), 2), 0); //Rescale amplitudes after update
	float scale = sqrt(ps1/ps2);
	O*=scale;

	R->updateGuess(O);
}

void HIO_Constraint::apply(Reconstruction* R)
{
	array O = R->getGuess();
	const array& S = R->getSupport();
	const array& oldO = R->getPreviousGuess();

	float ps1 = sum<float>(pow(abs(O), 2), 0);
	array phase = arg(O);

	replace(O, S && phase>-Pi/2 && phase<Pi/2, oldO-O*m_beta);

	float ps2 = sum<double>(pow(abs(O), 2), 0); //Rescale amplitudes after update
	float scale = sqrt(ps1/ps2);
	O*=scale;

	R->updateGuess(O);
}


void Shrinkwrap::apply(Reconstruction* R)
{
	const array& O = R->getGuess();
	array S = R->getSupport();

	array smoothed = gaussConvolve3(abs(O), m_gaussianSigma);
	S=(smoothed>=m_thresholdFactor*max<float>(smoothed));
	BlurConstraint::apply(R);
	R->updateSupport(S);

#ifdef DEBUG_MSG
	printf("Real space sigma %f\n", m_gaussianSigma);
#endif
}

void LRes::apply(Reconstruction* R)
{
	if(m_gaussianSigma>=1)
	{
		R->useCachedDiffs(false);
		m_gaussianSigma = 1;
	}
	else
	{
		const array& I = R->getNonCachedDiffs();
		R->updateCache(I*createGaussian(I.dims(),m_gaussianSigma));
		BlurConstraint::apply(R);
		R->useCachedDiffs(true);
	}
}

void TwinDeletion::apply(Reconstruction* R)
{
	const array& S = R->getSupport();
	dim4 size = S.dims();
	array SMask = constant(0, size, S.type());
	SMask(seq(size[0]/2,end),span,span) = 1;
	R->updateSupport(S*SMask);
}

} /* namespace FPL_NS */
