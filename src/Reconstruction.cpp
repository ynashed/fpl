/*
 * Reconstruction.cpp
 *
 *  Created on: Sep 10, 2015
 *      Author: ynashed
 */

#include "Reconstruction.h"
#include "FileManager.h"
#include "Vector3D.h"
#include "Parameters.h"

namespace fpl
{

Reconstruction::Reconstruction() : m_iterationsNum(0), m_useCachedDiffs(false)
{
	reloadData();
	initSupport();
	initObject();
	reloadConstraints();
}

void Reconstruction::reloadData()
{
	ExperimentParams* eParams = Params->getExperimentParams();
	PreprocessingParams* pParams = Params->getPreprocessingParams();

	if(m_diffData.initFromDir(eParams->dPath.c_str()) != 0)
		return;

	m_diffData.preprocess(	dim4(pParams->cropping[0], pParams->cropping[1], pParams->cropping[2]),
							dim4(pParams->binning[0], pParams->binning[1], pParams->binning[2]),
							pParams->aliens, pParams->threshold, pParams->sqrt, pParams->fftshift,
							pParams->rotate90, pParams->pad2, pParams->photons);
}

void Reconstruction::initObject(const af::array* guess)
{
	if(guess == 0)
	{
		const dim4 shape = m_diffData.shape();

		randomEngine generator(AF_RANDOM_ENGINE_DEFAULT,rand());
		m_object = m_support * exp(	af::complex(constant(0,shape,f32), randu(shape,f32,generator)-0.5));

		double ps1 = sum<double>(pow(abs(m_object), 2), 0);
		double ps2 = m_diffData.normalizedPowerSpectrum();
		double scale = sqrt(ps2/ps1);

		m_object*=scale;
		m_previousObject = m_object.copy();
	}
	else
		m_object = guess->copy();
}

void Reconstruction::initSupport(const af::array* guess)
{
	if(guess == 0)
	{
		const dim4 shape = m_diffData.shape();
		ReconstructionParams* rParams = Params->getReconstructionParams();
		m_support = constant(0, shape, b8);
		if(rParams->xf<1.0f || rParams->yf<1.0f || rParams->zf<1.0f)
		{
			dim4 fraction, midPoint(shape[0]/2, shape[1]/2, shape[2]/2);
			fraction[0] = floor(rParams->xf*midPoint[0]);
			fraction[1] = floor(rParams->yf*midPoint[1]);
			fraction[2] = floor(rParams->zf*midPoint[2]);
			dim4 start=midPoint-fraction, end=midPoint+fraction;
			m_support(seq(start[0], end[0]-1), seq(start[1], end[1]-1), seq(start[2], end[2]-1)) = 1;
		}
		else
			m_support = constant(1, shape, b8);
	}
	else
		m_support = guess->copy();
}

void Reconstruction::clearConstraints()
{
	for(size_t m=0; m<m_phasingConstraints.size(); ++m)
		if(m_phasingConstraints[m].constraint)
			delete m_phasingConstraints[m].constraint;
	m_phasingConstraints.clear();
}

void Reconstruction::reloadConstraints()
{
	ReconstructionParams* rParams = Params->getReconstructionParams();

	clearConstraints();
	//Add constraints from params
	ostringstream cName;
	cName << "lres:" << rParams->lres_sigma_start << " " << rParams->lres_sigma_end;
	addConstraint(cName.str().c_str(), rParams->lres_trigger[0], rParams->lres_trigger[1], rParams->lres_trigger[2]);
	cName.str("");

	addConstraint("modulus", 1, rParams->iterations);
	int startIteration = 1;
	for(size_t i=0; i<rParams->trigger.size(); ++i)
	{
		addConstraint(rParams->alg[i%rParams->alg.size()].c_str(), startIteration, rParams->trigger[i]);
		startIteration = rParams->trigger[i]+1;
		if(startIteration>=rParams->iterations)
			break;
	}
	if(startIteration<rParams->iterations)
		addConstraint(rParams->alg[0].c_str(), startIteration, rParams->iterations);

	cName << "shrinkwrap:" << rParams->sw_sigma_start << " " << rParams->sw_sigma_end << " " << rParams->sw_threshold;
	addConstraint(cName.str().c_str(), rParams->sw_trigger[0], rParams->sw_trigger[1], rParams->sw_trigger[2]);
	cName.str("");
}

void Reconstruction::updateGuess(const af::array& O)
{
	m_previousObject = m_object.copy(); //cache object
	m_object = O;
}

void Reconstruction::reconstruct()
{
	if(!m_diffData.isInitialized())
		return;
	for(int it=1; it<=m_iterationsNum; ++it)
	{
		for(size_t i=0; i<m_phasingConstraints.size(); ++i)
		{
			RecConstraint currentConstraint = m_phasingConstraints[i];
			if(		it>=currentConstraint.startIteration &&
					it<=currentConstraint.endIteration &&
					it%currentConstraint.frequency==0	)
				currentConstraint.constraint->apply(this);
			//deviceGC();
		}
	}

	//Center reconstruction
	dim4 offset = centerOfMass3(abs(m_object)*m_support);
	m_object = af::shift(m_object, 		(shape()[0]/2)-offset[0],
	                              		(shape()[1]/2)-offset[1],
	                              		(shape()[2]/2)-offset[2]);
	m_support = af::shift(m_support,	(shape()[0]/2)-offset[0],
	                                	(shape()[1]/2)-offset[1],
		                            	(shape()[2]/2)-offset[2]);

	//Removing phase offset
	float phi0 = sum<float>(arg(m_object(shape()[0]/2, shape()[0]/2, shape()[2]/2)));
	array phaseOffset = af::complex( af::constant(0,shape()),  af::constant(-phi0,shape()));
	m_object *= exp(phaseOffset);
	m_object /= max<float>(abs(m_object));
}

void Reconstruction::save()
{
	if(!m_diffData.isInitialized())
		return;
	ExperimentParams* eParams = Params->getExperimentParams();
	array axis = flat(range(shape(), 0)*(1.0/shape()[0]));			//X-Axis
	axis = (float(shape()[0]-1)/float(shape()[0])) - axis;			//The 1st coordinate sometimes gets an extra bin
	axis = join(1, axis, flat(range(shape(), 1)*(1.0/shape()[1]))); //Y-Axis
	axis = join(1, axis, flat(range(shape(), 2)*(1.0/shape()[2]))); //Z-Axis

	axis = axis.T();
	float* axis_h = axis.host<float>();

	if(eParams->labframeTransorm)
	{
		float deg2rad=Pi/180.0;
		float dpx = eParams->dxd/eParams->dz, dpy = eParams->dyd/eParams->dz;
		float delta = eParams->delta * deg2rad;
		float gamma = eParams->gamma * deg2rad;
		float dth = eParams->dth * deg2rad;

		Vector3D<float> dQdpx(-cos(delta)*cos(gamma),	0.0, 			sin(delta)*cos(gamma));
		Vector3D<float> dQdpy(sin(delta)*sin(gamma),	-cos(gamma),	cos(delta)*sin(gamma));
		Vector3D<float> dQdth(1-cos(delta)*cos(gamma),	0.0,			sin(delta)*cos(gamma));

		Vector3D<float> A=dQdpx*(2.0*Pi/eParams->lambda)*dpx;
		Vector3D<float> B=dQdpy*(2.0*Pi/eParams->lambda)*dpy;
		Vector3D<float> C=dQdth*(2.0*Pi/eParams->lambda)*dth;
		float denom = dot(A, cross(B,C));
		Vector3D<float> TA=cross(B,C)*2.0*Pi/denom;
		Vector3D<float> TB=cross(C,A)*2.0*Pi/denom;
		Vector3D<float> TC=cross(A,B)*2.0*Pi/denom;

		// TODO: someday make it a parallel dot product when arrayfire supports it
		for(size_t p=0; p<axis.dims(1); ++p)
		{
			Vector3D<float> point(axis_h[p*3], axis_h[(p*3)+1], axis_h[(p*3)+2]);
			axis_h[p*3] 	= dot(point, TA);
			axis_h[p*3+1] 	= dot(point, TB);
			axis_h[p*3+2] 	= dot(point, TC);
		}
	}

	IO::getInstance()->writeToVTKs<float>((eParams->jobID + ".vts").c_str(),
										m_object, axis_h, axis.bytes());

	ofstream infile((eParams->jobID + ".bin").c_str(),ofstream::out|ofstream::binary );
	cfloat* h_object =  new cfloat[m_object.elements()];
	m_object.host(h_object);
	infile.write((char*)h_object, m_object.elements()*sizeof(cfloat));
	infile.close();
	delete[] h_object;

	bool* h_support =  new bool[m_support.elements()];
	infile.open((eParams->jobID + "_support.bin").c_str(),ofstream::out|ofstream::binary);
	m_support.host(h_support);
	infile.write((char*)h_support, m_support.elements()*sizeof(bool));
	infile.close();
	delete[] h_support;

	infile.open((eParams->jobID + ".meta").c_str(),ofstream::out);
	infile << shape()[2] << ", " << shape()[0] << ", " << shape()[1] << endl;
	infile.close();
}

double Reconstruction::getModulusError() const
{
	double error = 0;
	for(size_t i=0; i<m_phasingConstraints.size(); ++i)
	{
		IConstraint* constraintPtr = m_phasingConstraints[i].constraint;
		if(constraintPtr->getName() == "modulus")
			error = (dynamic_cast<ModulusConstraint*>(constraintPtr))->getError();
	}
	return error;
}

double Reconstruction::getSharpness() const
{
	return sum<double>(pow(abs(m_object),4));
}

unsigned int Reconstruction::getVolume(float threshold) const
{
	array mag = abs(m_object);
	mag /= max<float>(mag);
	return af::count<float>(mag>=threshold);
}

} /* namespace FPL_NS */
