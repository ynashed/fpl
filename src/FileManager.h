/*
 * FileManager.h
 *
 *  Created on: Feb 19, 2016
 *      Author: ynashed
 */

#ifndef FILEMANAGER_H_
#define FILEMANAGER_H_

#include "Singleton.h"
#include "Utils.h"
#include "xmlwriter.h"
#include <af/array.h>
#include <fstream>

using namespace std;
using namespace	xmlw;
using namespace af;

namespace fpl
{

class FileManager
{
public:
	FileManager(){}
	virtual ~FileManager(){}

	template<typename T>
	void writeToVTKs(const char* fname, const array& data, const float* axis, size_t bytes);
};

template<typename T>
void FileManager::writeToVTKs(const char* fname, const array& data, const float* axis, size_t bytes)
{
	char extents[1024];
	dim4 dataSize = data.dims();
	sprintf(extents, "0 %d 0 %d 0 %d", (int)dataSize[0]-1,(int)dataSize[1]-1,(int)dataSize[2]-1);
	array objData = arg(data);
	size_t offset = bytes + sizeof(size_t);

	ofstream f(fname);
	XmlStream xml(f);
	xml << prolog() // write XML file declaration
	    << tag("VTKFile")
	    	<< attr("byte_order") << (isLittleEndian()?"LittleEndian":"BigEndian")
	    	<< attr("version") << "1.0"
	    	<< attr("type") << "StructuredGrid"
	    	<< attr("header_type") << "UInt64"
	    	<< tag("StructuredGrid")
	    		<< attr("WholeExtent") << string(extents)
	    		<< tag("Piece")
	    			<< attr("Extent") << string(extents)
					<< tag("Points")
						<< tag("DataArray")
							<< attr("type") << "Float32"
							<< attr("NumberOfComponents") << "3"
							<< attr("Name") << "coords"
							<< attr("format") << "appended"
							<< attr("offset") << 0
						<< endtag()
					<< endtag()
					<< tag("PointData")
						<< attr("scalars") << "Phase"
						<< tag("DataArray")
							<< attr("type") << "Float32"
							<< attr("NumberOfComponents") << "1"
							<< attr("Name") << "Phase"
							<< attr("format") << "appended"
							<< attr("offset") << offset
						<< endtag()
						<< tag("DataArray")
							<< attr("type") << "Float32"
							<< attr("NumberOfComponents") << "1"
							<< attr("Name") << "Intensity"
							<< attr("format") << "appended"
							<< attr("offset") << (offset+objData.bytes()+sizeof(size_t))
						<< endtag()
					<< endtag()
				<< endtag()
			<< endtag()
			<< tag("AppendedData")
				<< attr("encoding") << "raw" << chardata() << '_';
	f.write((char*)&bytes, sizeof(size_t));
	f.write((char*)axis, bytes);
	bytes = objData.bytes();
	f.write((char*)&bytes, sizeof(size_t));
	f.write((char*)objData.host<T>(), bytes);
	objData = abs(data);
	bytes = objData.bytes();
	f.write((char*)&bytes, sizeof(size_t));
	f.write((char*)objData.host<T>(), bytes);
}

typedef Singleton<FileManager> IO;

} /* namespace fpl */

#endif /* FILEMANAGER_H_ */
