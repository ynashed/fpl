/*
 * Utils.h
 *
 *  Created on: Dec 9, 2015
 *      Author: ynashed
 */

#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <arrayfire.h>

namespace fpl
{

inline std::string toLower(const std::string& s)
{
	std::string result(s);
	transform(result.begin(), result.end(), result.begin(), ::tolower);
	return result;
}

inline af::array createGaussian(af::dim4 dims, float sigma)
{
	af::array X = range(dims, 0)-dims[0]/2;
	af::array Y = range(dims, 1)-dims[1]/2;
	af::array Z = range(dims, 2)-dims[2]/2;
	X = sqrt(X*X+Y*Y+Z*Z);
	Y = 1;
	for(size_t i=0; i<dims.ndims(); ++i)
	{
		float s = sigma*dims[i];
		Y *= exp(-0.5*pow(X/s,2));
	}
    return Y;
}

inline af::array gaussConvolve3(const af::array& arr, float sigma)
{
	af::dim4 size = arr.dims();
	af::array gauss3D=createGaussian(size, sigma);
	gauss3D = shift(gauss3D, size[0]/2, size[1]/2, size[2]/2);
	return abs(ifft3(fft3(arr)*gauss3D));
}

inline void centerOfMass2(const af::array& arr, dim_t& x, dim_t& y)
{
	double m00, m01, m10;
	af::moments(&m00, arr, AF_MOMENT_M00);
	af::moments(&m01, arr, AF_MOMENT_M01);
	af::moments(&m10, arr, AF_MOMENT_M10);

	x = (dim_t)round(m10/m00);
	y = (dim_t)round(m01/m00);
}

inline af::dim4 centerOfMass3(const af::array& arr)
{
	af::dim4 center;

	af::array proj = af::sum(arr, 0);
	proj = af::moddims(proj, arr.dims(1), arr.dims(2));
	centerOfMass2(proj, center[2], center[1]);

	proj = af::sum(arr, 1);
	proj = af::moddims(proj, arr.dims(0), arr.dims(2));
	centerOfMass2(proj, center[2], center[0]);

	return center;
}

inline bool isLittleEndian()
{
    short int number = 0x1;
    char *numPtr = (char*)&number;
    return (numPtr[0] == 1);
}

} /* namespace fpl */

#endif /* SRC_UTILS_H_ */
