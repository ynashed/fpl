/*
 * DPStack.h
 *
 *  Created on: Aug 14, 2015
 *      Author: ynashed
 */

#ifndef DPSTACK_H_
#define DPSTACK_H_

#include <arrayfire.h>

namespace fpl
{

class DPStack
{
protected:
	af::array m_diffData;
	bool m_initialized;
	double m_powerSpectrum;

	void updatePowerSpectrum();
	template<typename T>
	void init(af::dim4 dims, const T* ptr, af::source src=afHost)
	{
		m_diffData = af::array(dims, ptr, src);
		updatePowerSpectrum();
		m_initialized = true;
	}

public:
	DPStack();
	virtual ~DPStack();

	//Initializers
	void initToZeros(af::dim4 dims, af::dtype type=f32);
	void initFromHost(af::dim4 dims, const float* hostPtr) {init<float>(dims, hostPtr);}
	void initFromHost(af::dim4 dims, const double* hostPtr){init<double>(dims, hostPtr);}
	void initFromDevice(af::dim4 dims, const float* devPtr){init<float>(dims, devPtr, afDevice);}
	void initFromDevice(af::dim4 dims,const double* devPtr){init<double>(dims, devPtr, afDevice);}
	int initFromImage(const char*);
	int initFromBin(const char*, unsigned int index=0);
	int initFromBin(const char*, af::dim4 dims);
	int initFromDir(const char*);

	void preprocess(af::dim4 adjust, af::dim4 bin, std::vector<std::vector<int> > aliens,
					int tao=0, bool takeSqrt=false, bool fftshift=false, bool rotate90=false,
					bool pad=true, double photons=-1);
	double powerSpectrum()			const {return m_powerSpectrum;}
	double normalizedPowerSpectrum()const {return m_powerSpectrum/m_diffData.elements();}
	af::dim4 shape() 				const {return m_diffData.dims();}
	const af::array& getArray() 	const {return m_diffData;}
	bool isInitialized() 			const {return m_initialized;}
	void addPoissonNoise(double photonCount);
};

} /* namespace fpl */

#endif /* DPSTACK_H_ */
