/*
 * DPStack.cpp
 *
 *  Created on: Aug 14, 2015
 *      Author: ynashed
 */

#include "DPStack.h"
#include "tinydir.h"
#include "Utils.h"
#include <fstream>

using namespace std;
using namespace af;

dim4 nextPowerOf2(dim4 size)
{
	for(int i=0;i<4;++i)
		size[i] = rint(pow(2.0, ceil(log2((double)size[i]))));
	return size;
}

namespace fpl
{

DPStack::DPStack(): m_diffData(), m_initialized(false), m_powerSpectrum(0.0)
{}

DPStack::~DPStack()
{}

void DPStack::updatePowerSpectrum()
{m_powerSpectrum = sum<double>(pow(m_diffData,2), 0);}

void DPStack::initToZeros(dim4 dims, dtype type)
{m_diffData = constant(0, dims, type);m_initialized = true;}

int DPStack::initFromImage(const char* fname)
{
	ifstream infile(fname,ofstream::in);
	if(!infile.is_open())
	{
		fprintf(stderr, "Can't open file (%s).\n", fname);
		return -1;
	}
	m_diffData = loadImage(fname);
	updatePowerSpectrum();
	m_initialized = true;
	return 0;
}

int DPStack::initFromBin(const char* fname, unsigned int index)
{
	ifstream infile(fname,ofstream::in|ofstream::binary );
	if(!infile.is_open())
	{
		fprintf(stderr, "Can't open file (%s).\n", fname);
		return -1;
	}
	m_diffData = readArray(fname, index);
	updatePowerSpectrum();
	m_initialized = true;
	return 0;
}

int DPStack::initFromBin(const char* fname, dim4 dims)
{
	ifstream infile(fname,ofstream::in|ofstream::binary );
	if(!infile.is_open())
	{
		fprintf(stderr, "Can't open file (%s).\n", fname);
		return -1;
	}
	size_t num = dims[0]*dims[1]*dims[2]*dims[3];
	float* h_array =  new float[num];
	infile.read((char*)h_array, num*sizeof(float));
	infile.close();

	initFromHost(dims, h_array);
	delete[] h_array;
	return 0;
}

int DPStack::initFromDir(const char* dirname)
{
	tinydir_dir dir;
	if(tinydir_open_sorted(&dir, dirname)<0)
	{
		fprintf(stderr, "Can't open directory (%s).\n", dirname);
		return -1;
	}
	for (int i=0; i<dir.n_files; ++i)
	{
		tinydir_file file;
		tinydir_readfile_n(&dir, &file, i);
		string ext = toLower(file.extension);
		string filepath = string(dir.path)+"/"+string(file.name);
		if(ext == "tiff" || ext == "tif")
		{
			//TODO: Use preallocated memory instead of incremental joining
			if(m_diffData.elements()==0)
				m_diffData = loadImage(filepath.c_str());
			else
				m_diffData = join(2, m_diffData, loadImage(filepath.c_str()));
		}
	}

	updatePowerSpectrum();
	m_initialized = true;
	return 0;
}

void DPStack::preprocess(af::dim4 adjust, af::dim4 bin, std::vector<std::vector<int> > aliens,
						int tao, bool takeSqrt, bool fftshift, bool rotate90, bool pad, double photons)
{
	if(!m_initialized)
		return;

	//Remove Aliens
	for(size_t i=0; i<aliens.size(); ++i)
		m_diffData(	seq(aliens[i][0],aliens[i][3]),
					seq(aliens[i][1],aliens[i][4]),
					seq(aliens[i][2],aliens[i][5])) = 0;

	if(tao>0)
		m_diffData(m_diffData<tao) = 0.0;
	if(takeSqrt)
		m_diffData = sqrt(m_diffData);
	if(rotate90)
		m_diffData = reorder(m_diffData,1,0);

	//Center according to max value
	float value;
	unsigned int index;
	max(&value, &index, m_diffData);
	dim4 dataSize = shape();
	dim4 maxIndex;
	maxIndex[2] = index/(dataSize[0]*dataSize[1]);
	index = index%(dataSize[0]*dataSize[1]);
	maxIndex[0] = index%dataSize[0];
	maxIndex[1] = index/dataSize[0];
	m_diffData = shift(m_diffData, dataSize[0]/2-maxIndex[0], dataSize[1]/2-maxIndex[1], dataSize[2]/2-maxIndex[2]);
	//Pad and Crop
	dim4 newSize = shape();
	newSize[0] += adjust[0]*2;
	newSize[1] += adjust[1]*2;
	newSize[2] += adjust[2]*2;
	array expanded = constant(0.0, newSize, m_diffData.type());

	dim4 dataOffset, outOffset, outSize;
	outOffset[0] = (adjust[0]<0)? abs(adjust[0]): 0;
	dataOffset[0]= (adjust[0]<0)? 0: adjust[0];
	outSize[0]	 = min(newSize[0], dataSize[0]);
	outOffset[1] = (adjust[1]<0)? abs(adjust[1]): 0;
	dataOffset[1]= (adjust[1]<0)? 0: adjust[1];
	outSize[1]	 = min(newSize[1], dataSize[1]);
	outOffset[2] = (adjust[2]<0)? abs(adjust[2]): 0;
	dataOffset[2]= (adjust[2]<0)? 0: adjust[2];
	outSize[2]	 = min(newSize[2], dataSize[2]);
	expanded(	seq(dataOffset[0],dataOffset[0]+outSize[0]-1),
			 	seq(dataOffset[1],dataOffset[1]+outSize[1]-1),
			 	seq(dataOffset[2],dataOffset[2]+outSize[2]-1))	=
	m_diffData(	seq(outOffset[0],outOffset[0]+outSize[0]-1),
			 	seq(outOffset[1],outOffset[1]+outSize[1]-1),
			 	seq(outOffset[2],outOffset[2]+outSize[2]-1));
	m_diffData = expanded;
	//

	//TODO: Binning
	if(pad)
	{
		dim4 size = shape();
		dim4 padded = nextPowerOf2(size);
		expanded = constant(0.0, padded, m_diffData.type());
		int shift_x=padded[0]/2-size[0]/2;
		int shift_y=padded[1]/2-size[1]/2;
		int shift_z=padded[2]/2-size[2]/2;
		expanded(seq(shift_x,shift_x+size[0]-1),seq(shift_y,shift_y+size[1]-1),seq(shift_z,shift_z+size[2]-1))=m_diffData;
		m_diffData = expanded;
	}
	if(fftshift)
		m_diffData = shift(m_diffData, shape()[0]/2, shape()[1]/2, shape()[2]/2);
	updatePowerSpectrum();

	if(photons>0)
		addPoissonNoise(photons);
}

void DPStack::addPoissonNoise(double photonCount)
{
	m_diffData /= max<float>(m_diffData);
	m_diffData *= photonCount;
	//TODO: use arrayfire poisson when available (now using an approximated Poisson distribution)
	m_diffData += (sqrt(m_diffData)*af::randn(shape()) - 0.5)+4;
	m_diffData(m_diffData<4) = 0;
}

} /* namespace FPL_NS */
