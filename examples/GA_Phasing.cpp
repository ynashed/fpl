/*
 * GA_Phasing.cpp
 *
 *  Created on: Jan 23, 2017
 *      Author: ynashed
 */

#include "Reconstruction.h"
#include "Parameters.h"
#include <stdio.h>
#include <mpi.h>

using namespace fpl;
using namespace af;

int findBestReconstruction(double myErr, int myRank)
{
	int bestRank = 0;
	struct
	{
		double val;
		int rank;
	} in, out;
	in.val=myErr; in.rank=myRank;
	MPI_Reduce(&in,&out,1,MPI_DOUBLE_INT,MPI_MINLOC,0,MPI_COMM_WORLD);
	if(myRank == 0)
		bestRank = out.rank;
	MPI_Bcast(&bestRank,1,MPI_INT,0,MPI_COMM_WORLD);
	return bestRank;
}

int main(int argc, char *argv[])
{
	int GENERATIONS = 10, metricID = 0;
	int myRank,nProcs;
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&myRank);
	MPI_Comm_size(MPI_COMM_WORLD,&nProcs);

	setDevice(myRank%getDeviceCount());

	if(argc<2)
	{
		fprintf(stderr, "Usage: mpiexec -n <population size> GA_Phasing_<backend> <params-file> [# of generations] [metric id]\n");
		MPI_Finalize();
		return 0;
	}
	if(argc >= 3)
	{
		GENERATIONS = atoi(argv[2]);
		if(argc == 4)
			metricID = atoi(argv[3]);
	}
	if (Params->loadFromFile(argv[1]) < 0)
	{
		fprintf(stderr, "Can't load parameters file (%s).\n", argv[1]);
		MPI_Finalize();
		return 0;
	}

	/////////
	srand(time(NULL)+myRank);
	Reconstruction r;
	int bestRank = 0;
	double myErr = 0;
	cfloat* bestRho = new cfloat[r.getGuess().elements()];

	timer::start();
	for(int g=0; g<GENERATIONS; ++g)
	{
		if(myRank == 0)
			fprintf(stdout, "Generation [%d], starting ...\n", g+1);

		r.reconstruct();

		switch(metricID)
		{
			case 0: myErr=r.getModulusError(); break;
			case 1: myErr=r.getSharpness(); break;
			case 2: myErr=pow(r.getSharpness(),0.25); break;
			case 3: myErr=1.0/r.getVolume(); break;
			default: myErr=r.getModulusError(); break;
		}

		bestRank = findBestReconstruction(myErr, myRank);
		if(myRank == bestRank)
			fprintf(stdout, "Best individual [%d], metric [%e], error [%e]\n", myRank, myErr, r.getModulusError());

		r.getGuess().host(bestRho);
		MPI_Bcast(bestRho,r.getGuess().elements(),MPI_COMPLEX,bestRank,MPI_COMM_WORLD);
		array bestO(r.shape(), bestRho);

		//TODO: subpixel 3D registeration of reconstructions, phase offset
		bestO = sqrt(r.getGuess()*bestO);
		r.reset(&bestO);
	}
	delete [] bestRho;

	if(myRank==bestRank)
	{
		printf("elapsed seconds: %g\n", timer::stop());
		r.save();
	}

	MPI_Finalize();
	return 0;
}


