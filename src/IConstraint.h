/*
 * IConstraint.h
 *
 *  Created on: Sep 29, 2015
 *      Author: ynashed
 */

#ifndef SRC_ICONSTRAINT_H_
#define SRC_ICONSTRAINT_H_

#include <arrayfire.h>
#include <sstream>
#include "Utils.h"

//#define DEBUG_MSG

namespace fpl
{

class Reconstruction;

class IConstraint
{
protected:
	int m_numIterations;
	std::string m_name;
	std::stringstream m_params;

	template<typename T>
	void getParam(T& toset)
	{
		if(!m_params.eof())
			m_params >> toset;
	}

public:
	IConstraint(const std::string& name, const std::string& p, int N):  m_numIterations(N),
																		m_name(name),
																		m_params(p)
	{}
	virtual ~IConstraint(){}

	virtual const std::string& getName() const {return m_name;}
	virtual void apply(Reconstruction*) = 0;
	static IConstraint* createInstance(const char* p, int N);
};
//

class ReciprocalspaceConstraint : public IConstraint
{
protected:
	double m_error;
	void transform(af::array& O)	{af::fft3InPlace(O);}
	void inverse(af::array& O)		{af::ifft3InPlace(O);}

public:
	ReciprocalspaceConstraint(const std::string& name, const std::string& p, int N):IConstraint(name,p,N),
	m_error(0)
	{}
	virtual ~ReciprocalspaceConstraint(){}

	virtual void update(af::array&, const af::array&) = 0;
	virtual void apply(Reconstruction*);
	double getError() const {return m_error;}
};
//

class ModulusConstraint : public ReciprocalspaceConstraint
{
public:
	ModulusConstraint(const std::string& name, const std::string& p, int N): ReciprocalspaceConstraint(name,p,N)
	{}
	virtual ~ModulusConstraint(){}

	virtual void update(af::array& O, const af::array& I)
	{O=(I*(O/af::abs(O)));} //Replace with measured amplitudes
};
//

class SupportConstraint : public IConstraint
{
public:
	SupportConstraint(const std::string& name, const std::string& p, int N): IConstraint(name,p,N)
	{}
	virtual ~SupportConstraint(){}

	virtual void apply(Reconstruction*);
};
//

class HIO_Constraint : public IConstraint
{
protected:
	float m_beta;

public:
	HIO_Constraint(const std::string& name, const std::string& p, int N): 	IConstraint(name,p,N),
																			m_beta(0.9f)
	{
		getParam(m_beta);
	}
	virtual ~HIO_Constraint(){}

	virtual void apply(Reconstruction*);
};
//

class BlurConstraint : public IConstraint
{
protected:
	float m_gaussianSigma;
	float m_gaussianSigmaEnd;
	float m_gaussianInc;

public:
	BlurConstraint(const std::string& name, const std::string& p, int N): 	IConstraint(name,p,N),
																			m_gaussianSigma(0.5f),
																			m_gaussianSigmaEnd(1.0f)
	{
		getParam(m_gaussianSigma);
		getParam(m_gaussianSigmaEnd);
		m_gaussianInc = (m_gaussianSigmaEnd-m_gaussianSigma)/m_numIterations;
	}
	virtual ~BlurConstraint(){}

	virtual void apply(Reconstruction*)
	{
		m_gaussianSigma+=m_gaussianInc;
	}
};

class Shrinkwrap : public BlurConstraint
{
protected:
	float m_thresholdFactor;

public:
	Shrinkwrap(const std::string& name, const std::string& p, int N): 	BlurConstraint(name,p,N),
																		m_thresholdFactor(0.2f)
	{
		getParam(m_thresholdFactor);
	}
	virtual ~Shrinkwrap(){}

	virtual void apply(Reconstruction*);
};
//

class LRes : public BlurConstraint
{
public:
	LRes(const std::string& name, const std::string& p, int N): BlurConstraint(name,p,N)
	{}
	virtual ~LRes(){}

	virtual void apply(Reconstruction*);
};
//

class TwinDeletion : public IConstraint
{
public:
	TwinDeletion(const std::string& name, const std::string& p, int N): IConstraint(name,p,N)
	{}
	virtual ~TwinDeletion(){}

	virtual void apply(Reconstruction*);
};

} /* namespace fpl */

#endif /* SRC_ICONSTRAINT_H_ */
