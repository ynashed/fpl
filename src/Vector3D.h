/*
 * Vector3D.h
 *
 *  Created on: Feb 24, 2016
 *      Author: ynashed
 */

#ifndef VECTOR3D_H_
#define VECTOR3D_H_

namespace fpl
{

template<typename T>
class Vector3D
{
public:
	T   x;
	T   y;
	T   z;

	Vector3D(T r=0, T s=0, T t=0): x(r), y(s), z(t)
	{}

	T& operator [](long k)
	{return ((&x)[k]);}

	const T& operator [](long k) const
	{return ((&x)[k]);}

	Vector3D& operator +=(const Vector3D& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		return (*this);
	}

	Vector3D& operator -=(const Vector3D& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return (*this);
	}

	Vector3D& operator *=(T t)
	{
		x *= t;
		y *= t;
		z *= t;
		return (*this);
	}

	Vector3D& operator /=(T t)
	{
		T f = 1.0 / t;
		x *= f;
		y *= f;
		z *= f;
		return (*this);
	}

	Vector3D& operator %=(const Vector3D& v)
	{
		T       r, s;

		r = y * v.z - z * v.y;
		s = z * v.x - x * v.z;
		z = x * v.y - y * v.x;
		x = r;
		y = s;

		return (*this);
	}

	Vector3D& operator *=(const Vector3D& v)
	{
		x *= v.x;
		y *= v.y;
		z *= v.z;
		return (*this);
	}

	Vector3D operator -() const
	{return (Vector3D(-x, -y, -z));}

	Vector3D operator +(const Vector3D& v) const
	{return (Vector3D(x + v.x, y + v.y, z + v.z));}

	Vector3D operator -(const Vector3D& v) const
	{return (Vector3D(x - v.x, y - v.y, z - v.z));}

	Vector3D operator *(T t) const
	{return (Vector3D(x * t, y * t, z * t));}

	Vector3D operator /(T t) const
	{
		T f = 1.0 / t;
		return (Vector3D(x * f, y * f, z * f));
	}

	T operator ^(const Vector3D& v) const
	{return (x * v.x + y * v.y + z * v.z);}

	Vector3D operator %(const Vector3D& v) const
	{
		return (Vector3D(y * v.z - z * v.y, z * v.x - x * v.z,
				x * v.y - y * v.x));
	}

	Vector3D operator *(const Vector3D& v) const
	{return (Vector3D(x * v.x, y * v.y, z * v.z));}

	bool operator ==(const Vector3D& v) const
	{return ((x == v.x) && (y == v.y) && (z == v.z));}

	bool operator !=(const Vector3D& v) const
	{return ((x != v.x) || (y != v.y) || (z != v.z));}

	Vector3D& normalize(void)
	{return (*this /= sqrt(x * x + y * y + z * z));}
};

template<typename T>
inline Vector3D<T> operator *(T t, const Vector3D<T>& v)
{return (Vector3D<T>(t * v.x, t * v.y, t * v.z));}

template<typename T>
inline T dot(const Vector3D<T>& v1, const Vector3D<T>& v2)
{return (v1 ^ v2);}

template<typename T>
inline Vector3D<T> cross(const Vector3D<T>& v1, const Vector3D<T>& v2)
{return (v1 % v2);}

template<typename T>
inline T mag(const Vector3D<T>& v)
{return (sqrt(v.x * v.x + v.y * v.y + v.z * v.z));}

template<typename T>
inline T squaredMag(const Vector3D<T>& v)
{return (v.x * v.x + v.y * v.y + v.z * v.z);}

}/* namespace fpl */

#endif /* VECTOR3D_H_ */
