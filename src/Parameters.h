/*
 * Parameters.h
 *
 *  Created on: Feb 25, 2016
 *      Author: ynashed
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

#include "Singleton.h"
#include <string>
#include <vector>

namespace fpl
{

struct PreprocessingParams
{
	int binning[3];
	int cropping[3];
	int threshold;
	std::vector< std::vector<int> > aliens;

	bool sqrt;
	bool fftshift;
	bool rotate90;
	bool pad2;
	double photons;

	PreprocessingParams() : threshold(0), sqrt(true), fftshift(true), rotate90(false), pad2(true), photons(0)
	{}
};

struct ExperimentParams
{
	//All units are in nm
	double dxd;				//detector pixel size
	double dyd;
	double dz;				//detector distance
	double lambda;			//Wavelength of x-ray
	//Angles are in degrees
	double delta;			//Detector angles
	double gamma;
	double dth;      		//step values for th translation
	double dtilt;

	bool labframeTransorm;

	std::string jobID;		//reconstruction ID
	std::string dPath;		//path of tiff files

	ExperimentParams() : dxd(55e3), dyd(55e3), dz(0.635e9),
						lambda(0.13933), delta(30.2), gamma(14.32),
						dth(0.010), dtilt(0), labframeTransorm(true)
	{}
};

struct ReconstructionParams
{
	//Initial box support (in fractions of data array size)
	double xf;
	double yf;
	double zf;

	int iterations;
	std::vector<std::string> alg;
	std::vector<int> trigger;

	double sw_threshold;                //Intensity threshold for shrinkwrap (0.0-1.0)
	double sw_sigma_start;              //Initial shrink_wrap sigma
	double sw_sigma_end;  	            //Final shrink_wrap sigma
	int sw_trigger[3];
	double lres_sigma_start;			//What fraction of data array to use initially--this is in array fraction
	double lres_sigma_end;				//What final fraction to use, this should always be 1.0
	int lres_trigger[3];

	ReconstructionParams(): xf(1),yf(1),zf(1), iterations(0), sw_threshold(0.1), sw_sigma_start(0.1), sw_sigma_end(0.001),
							lres_sigma_start(0.1), lres_sigma_end(1)
	{}
};

class Parameters
{
private:
	ExperimentParams m_eParams;
	PreprocessingParams m_pParams;
	ReconstructionParams m_rParams;

public:
	Parameters(){}
	virtual ~Parameters(){}

	ExperimentParams*		getExperimentParams()	{return &m_eParams;}
	PreprocessingParams*	getPreprocessingParams(){return &m_pParams;}
	ReconstructionParams*	getReconstructionParams(){return &m_rParams;}

	int loadFromFile(const char* fname);
};

#define Params Singleton<Parameters>::getInstance()

} /* namespace fpl */

#endif /* PARAMETERS_H_ */
