/*
 * test.cpp
 *
 *  Created on: Aug 14, 2015
 *      Author: ynashed
 */

#include "Reconstruction.h"
#include "Parameters.h"
#include <stdio.h>

using namespace fpl;
using namespace af;

int main(int argc, char *argv[])
{
	if(argc<2)
	{
		fprintf(stderr, "Usage: fpl-test_<backend> <params-file>\n");
		return 1;
	}

	if (Params->loadFromFile(argv[1]) < 0)
	{
		fprintf(stderr, "Can't load parameters file (%s).\n", argv[1]);
		return -1;
	}

	Reconstruction r;

	timer::start();
	r.reconstruct();
	printf("elapsed seconds: %g\n", timer::stop());

	r.save();

	return 0;
}

