/*
 * Parameters.cpp
 *
 *  Created on: Apr 25, 2016
 *      Author: ynashed
 */

#include "Parameters.h"
#include "INIReader.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

using namespace std;

void parseVals(const char* str, vector<string>& result)
{
	string copy(str);
	copy.erase(std::remove(copy.begin(), copy.end(), ')'), copy.end());
	copy.erase(std::remove(copy.begin(), copy.end(), '('), copy.end());
	stringstream ss(copy);

	result.clear();
	while (getline(ss, copy, ','))
		result.push_back(copy);
}

void parseVals(const char* str, vector<vector<int> >& result, int rank)
{
	vector<string> strResult;
	parseVals(str, strResult);
	result.clear();
	int numVals = 0;
	for(size_t i=0; i<strResult.size(); ++i)
	{
		if(numVals%rank==0)
			result.push_back(vector<int>());
		result.back().push_back(atoi(strResult[i].c_str()));
		numVals++;
	}
}

namespace fpl
{

int Parameters::loadFromFile(const char* fname)
{

    INIReader reader(fname);

    if (reader.ParseError() < 0)
        return reader.ParseError();

    m_eParams.dxd = reader.GetReal("Experiment", "det_px", m_eParams.dxd);
    m_eParams.dyd = m_eParams.dxd;//reader.GetReal("Experiment", "det_py", m_eParams.dyd);
    m_eParams.dz = reader.GetReal("Experiment", "arm", m_eParams.dz);
    m_eParams.lambda = reader.GetReal("Experiment", "lam", m_eParams.lambda);
    m_eParams.delta = reader.GetReal("Experiment", "delta", m_eParams.delta);
    m_eParams.gamma = reader.GetReal("Experiment", "gam", m_eParams.gamma);
    m_eParams.dth = reader.GetReal("Experiment", "dth", m_eParams.dth);
    m_eParams.dtilt = reader.GetReal("Experiment", "dtilt", m_eParams.dtilt);
    m_eParams.labframeTransorm = reader.GetBoolean("Experiment", "labframe_transformation", m_eParams.labframeTransorm);
    m_eParams.jobID = reader.Get("Experiment", "ID", "UNKNOWN");
    m_eParams.dPath = reader.Get("Experiment", "data", "UNKNOWN");

    vector<vector<int> > intVals;

    //3 bin values
    parseVals(reader.Get("Preprocessing", "binning", "UNKNOWN").c_str(), intVals, 3);
    m_pParams.binning[0] = intVals[0][0];m_pParams.binning[1] = intVals[0][1];m_pParams.binning[2] = intVals[0][2];
    //3 crop values
	parseVals(reader.Get("Preprocessing", "cropping", "UNKNOWN").c_str(), intVals, 3);
	m_pParams.cropping[0] = intVals[0][0];m_pParams.cropping[1] = intVals[0][1];m_pParams.cropping[2] = intVals[0][2];
	//Aliens of 6 values each
	parseVals(reader.Get("Preprocessing", "aliens", "UNKNOWN").c_str(), intVals, 6);
	m_pParams.aliens = intVals;
    m_pParams.threshold = reader.GetInteger("Preprocessing", "threshold", m_pParams.threshold);
    m_pParams.sqrt = reader.GetBoolean("Preprocessing", "sqrt", m_pParams.sqrt);
    m_pParams.fftshift = reader.GetBoolean("Preprocessing", "fftshift", m_pParams.fftshift);
    m_pParams.rotate90 = reader.GetBoolean("Preprocessing", "rotate90", m_pParams.rotate90);
    m_pParams.pad2 = reader.GetBoolean("Preprocessing", "pad2", m_pParams.pad2);
    m_pParams.photons = reader.GetReal("Preprocessing", "total_photons", m_pParams.photons);

    m_rParams.xf = reader.GetReal("Reconstruction", "xf", m_rParams.xf);
    m_rParams.yf = reader.GetReal("Reconstruction", "yf", m_rParams.yf);
    m_rParams.zf = reader.GetReal("Reconstruction", "zf", m_rParams.zf);
    m_rParams.iterations = reader.GetInteger("Reconstruction", "iterations", m_rParams.iterations);
    parseVals(reader.Get("Reconstruction", "ALG", "UNKNOWN").c_str(), m_rParams.alg);
    parseVals(reader.Get("Reconstruction", "trigger", "UNKNOWN").c_str(), intVals, 1);
    for(size_t i=0; i<intVals.size(); ++i)
    	m_rParams.trigger.push_back(intVals[i][0]);
    m_rParams.sw_threshold = reader.GetReal("Reconstruction", "sw_threshold", m_rParams.sw_threshold);
    m_rParams.sw_sigma_start = reader.GetReal("Reconstruction", "sw_sigma_start", m_rParams.sw_sigma_start);
    m_rParams.sw_sigma_end = reader.GetReal("Reconstruction", "sw_sigma_end", m_rParams.sw_sigma_end);
    parseVals(reader.Get("Reconstruction", "sw_trigger", "UNKNOWN").c_str(), intVals, 3);
    m_rParams.sw_trigger[0] = intVals[0][0];m_rParams.sw_trigger[1] = intVals[0][1];m_rParams.sw_trigger[2] = intVals[0][2];
    m_rParams.lres_sigma_start = reader.GetReal("Reconstruction", "lres_sigma_start", m_rParams.lres_sigma_start);
    m_rParams.lres_sigma_end = reader.GetReal("Reconstruction", "lres_sigma_end", m_rParams.lres_sigma_end);
    parseVals(reader.Get("Reconstruction", "lres_trigger", "UNKNOWN").c_str(), intVals, 3);
    m_rParams.lres_trigger[0] = intVals[0][0];m_rParams.lres_trigger[1] = intVals[0][1];m_rParams.lres_trigger[2] = intVals[0][2];

    return reader.ParseError();
}

}
