#!/usr/bin/python

import sys, getopt
import numpy as np
import scipy.io as sio

def main(argv):
	reconID = ''
	if len(argv) < 1:
		print 'usage: convertToMat.py <reconstructionID>'
		sys.exit(1)
	else:
		reconID = argv[0]
		size = np.genfromtxt(reconID+'.meta', delimiter=',', dtype=np.int)
		obj = np.fromfile(reconID+'.bin', np.complex64).reshape(size)
		supp = np.fromfile(reconID+'_support.bin', np.bool8).reshape(size)
		sio.savemat(reconID+'.mat', mdict={'rho':obj, 'support':supp})

if __name__ == "__main__":
   main(sys.argv[1:])